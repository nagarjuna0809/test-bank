# test-bank

Basic Bank App
# Start Fabric
 nagarjuna0809@ubuntu-trusty-1:~/fabric-dev-servers$     cd ~/fabric-dev-servers
 nagarjuna0809@ubuntu-trusty-1:~/fabric-dev-servers$     ./startFabric.sh
# Create Business Nextwork Definition
```
nagarjuna0809@ubuntu-trusty-1:~/fabric-dev-servers$ yo hyperledger-composer
Welcome to the Hyperledger Composer project generator
? Please select the type of project: Business Network
You can run this generator using: 'yo hyperledger-composer:businessnetwork'
Welcome to the business network generator
? Business network name: test-bank
? Description: Basic Bank App
? Author name:  Nagarjuna Bandamedi
? Author email: nagarjuna0809@gmail.com
? License: Apache-2.0
? Namespace: test
? Do you want to generate an empty template network? No: generate a populated sample network
   create package.json
namespace test
   create README.md
/**
   create models/test.cto
   create permissions.acl
   create .eslintrc.yml
   create features/sample.feature
   create features/support/index.js
   create test/logic.js
   create lib/logic.js
   */
```
# Modify Model and Logic
```
nagarjuna0809@ubuntu-trusty-1:~/fabric-dev-servers$ cd test-bank
nagarjuna0809@ubuntu-trusty-1:~/fabric-dev-servers/test-bank$ ls
features  lib  models  package.json  permissions.acl  README.md  test
```
#### Model
```
nagarjuna0809@ubuntu-trusty-1:~/fabric-dev-servers/test-bank$ vi models/test.cto
```
#### Logic
```
nagarjuna0809@ubuntu-trusty-1:~/fabric-dev-servers/test-bank$ vi lib/logic.js
```
# Generate a business network archive
```
nagarjuna0809@ubuntu-trusty-1:~/fabric-dev-servers/test-bank$ composer archive create -t dir -n .
Creating Business Network ArchiveLooking for package.json of Business Network Definition Input directory: /home/nagarjuna0809/fabric-dev-servers/test-bankFound: Description: Basic Bank App Name: test-bank Identifier: test-bank@0.0.1Written Business Network Definition Archive file to Output file: test-bank@0.0.1.bnaCommand succeeded
```

# Deploying the business network

#### Install the business network
```
nagarjuna0809@ubuntu-trusty-1:~/fabric-dev-servers/test-bank$ composer network install --card PeerAdmin@hlfv1 --archiveFile test-bank@0.0.1.bna
✔ Installing business network. This may take a minute...
Successfully installed business network test-bank, version 0.0.1

Command succeeded
```
#### Start the business network
```
nagarjuna0809@ubuntu-trusty-1:~/fabric-dev-servers/test-bank$ composer network start --networkName test-bank --networkVersion 0.0.1 --networkAdmin admin --networkAdminEnrollSecret adminpw --card PeerAdmin@hlfv1 --file bankadmin.card
Starting business network test-bank at version 0.0.1

Processing these Network Admins: 
        userName: admin

✔ Starting business network definition. This may take a minute...
Successfully created business network card:
        Filename: admin@test-bank.card

Command succeeded
```
#### Import the network administrator identity as a usable business network card
```
nagarjuna0809@ubuntu-trusty-1:~/fabric-dev-servers/test-bank$ composer card import --file bankadmin.card

Successfully imported business network card
        Card file: bankadmin.card
        Card name: admin@test-bank

Command succeeded
```
####  Check that the business network has been deployed successfully
```
nagarjuna0809@ubuntu-trusty-1:~/fabric-dev-servers/test-bank$ composer network ping --card admin@test-bank
The connection to the network was successfully tested: test-bank
        Business network version: 0.0.1
        Composer runtime version: 0.19.5
        participant: org.hyperledger.composer.system.NetworkAdmin#admin
        identity: org.hyperledger.composer.system.Identity#212868a36c56e90d306a8592e684515284b2bf76030678df7ceda93c5863da61

Command succeeded
```
# Generating a REST server
### Option 1
```
nagarjuna0809@ubuntu-trusty-1:~/fabric-dev-servers/test-bank$ composer-rest-server
? Enter the name of the business network card to use: admin@test-bank
? Specify if you want namespaces in the generated REST API: always use namespaces
? Specify if you want to use an API key to secure the REST API: No
? Specify if you want to enable authentication for the REST API using Passport: No
? Specify if you want to enable event publication over WebSockets: Yes
? Specify if you want to enable TLS security for the REST API: No

To restart the REST server using the same options, issue the following command:
   composer-rest-server -c admin@test-bank -n always -w true

Discovering types from business network definition ...
Discovered types from business network definition
Generating schemas for all types in business network definition ...
Generated schemas for all types in business network definition
Adding schemas for all types to Loopback ...
Added schemas for all types to Loopback
Web server listening at: http://localhost:3000
Browse your REST API at http://localhost:3000/explorer
```
### Option 2
```
nagarjuna0809@ubuntu-trusty-1:~/fabric-dev-servers/test-bank$ composer-rest-server -c admin@test-bank -n never -p 8080
Discovering types from business network definition ...
Discovered types from business network definition
Generating schemas for all types in business network definition ...
Generated schemas for all types in business network definition
Adding schemas for all types to Loopback ...
Added schemas for all types to Loopback
Web server listening at: http://localhost:8080
Browse your REST API at http://localhost:8080/explorer
```
# Demo
#### Add Customers : 
```

curl -X POST --header 'Content-Type: application/json' --header 'Accept: application/json' -d '{ \ 
   "$class": "test.Customer", \ 
   "customerId": "nagarjuna", \ 
   "firstName": "Nagarjuna", \ 
   "lastName": "B" \ 
 }' 'http://35.224.53.118:3000/api/test.Customer'

curl -X POST --header 'Content-Type: application/json' --header 'Accept: application/json' -d '{ \ 
   "$class": "test.Customer", \ 
   "customerId": "sandeep", \ 
   "firstName": "Sandeep", \ 
   "lastName": "Bo" \ 
 }' 'http://35.224.53.118:3000/api/test.Customer'


curl -X POST --header 'Content-Type: application/json' --header 'Accept: application/json' -d '{ \ 
   "$class": "test.Account", \ 
   "accountId": "nagarjunaAccount", \ 
   "owner": "nagarjuna", \ 
   "balance": 100 \ 
 }' 'http://35.224.53.118:3000/api/test.Account'

curl -X POST --header 'Content-Type: application/json' --header 'Accept: application/json' -d '{ \ 
   "$class": "test.Account", \ 
   "accountId": "sandeepAccount", \ 
   "owner": "sandeep", \ 
   "balance": 100 \ 
 }' 'http://35.224.53.118:3000/api/test.Account'



curl -X POST --header 'Content-Type: application/json' --header 'Accept: application/json' -d '{ \ 
   "$class": "test.AccountTransfer", \ 
   "from": "nagarjunaAccount", \ 
   "to": "sandeepAccount", \ 
   "amount": 30 \ 
 }' 'http://35.224.53.118:3000/api/test.AccountTransfer'

Response Body
{
  "$class": "test.AccountTransfer",
  "from": "nagarjunaAccount",
  "to": "sandeepAccount",
  "amount": 30,
  "transactionId": "84a0cff7414c377e5b647137a446b65d205bb80b5657f1b4205a75f1d3331544"
}

```

