/**
* Sample transaction
* @param {test.AccountTransfer} accountTransfer
* @transaction
*/
function accountTransfer(accountTransfer) {
if (accountTransfer.from.balance < accountTransfer.amount) {
throw new Error ("Insufficient funds");
}
accountTransfer.from.balance -= accountTransfer.amount;
accountTransfer.to.balance += accountTransfer.amount;
return getAssetRegistry('test.Account')
.then (function (assetRegistry) {
return assetRegistry.update(accountTransfer.from);
})
.then (function () {
return getAssetRegistry('test.Account');
})
.then(function (assetRegistry) {
return assetRegistry.update(accountTransfer.to);
}).then(function (assetRegistry) {
 var factory = getFactory();
 var basicEvent = factory.newEvent('test', 'BasicEvent');
 basicEvent.from=accountTransfer.from;
 basicEvent.to=accountTransfer.to;
 basicEvent.amount=accountTransfer.amount;
 emit(basicEvent);
});
}
